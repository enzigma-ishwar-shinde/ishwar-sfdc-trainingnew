public class CandidateTriggersHandler { 
    
    public static void addDate(List<Candidate__c> lstCand){ 
        List<Candidate__c> candidateList = new List<Candidate__c>();
        Set<Id> newCandidateIds = new Set<Id>();
        
   		for(Candidate__c objCand : lstcand) {
            if(objCand.Application_Date__c == null || objCand.Application_Date__c != objCand.CreatedDate) {
                newCandidateIds.add(objCand.Id);
            }
        }
        
        List<Candidate__c> candidateRecordsToBeUpdated = [SELECT Application_Date__c, CreatedDate FROM Candidate__c WHERE Id IN: newCandidateIds];
        
        for(Candidate__c candidateToBeUpdated : candidateRecordsToBeUpdated) {
            candidateToBeUpdated.Application_Date__c = candidateToBeUpdated.CreatedDate.date();
            //candidateToBeUpdated.Application_Date__c = DATEVALUE(candidateToBeUpdated.CreatedDate);
        }
        update candidateRecordsToBeUpdated;
    }
    
    public static void checkActive(List<Candidate__c> lstCand){
        Map <Id, Job__c> notActiveJob= new Map<Id, Job__c>([SELECT Active__c FROM Job__c WHERE Active__c=:false]);
        //System.debug(notActiveJob);
        for(Candidate__c objCand: lstCand){
            //System.debug(notActiveJob.get(data.Job__c));
            if(notActiveJob.get(objCand.Job__c) != null){
                objCand.addError('This Job is not active. You can not apply for this job');
            }
        }
    }
    
    public static void checkSalary(List<Candidate__c> lstCand){
        for(Candidate__c objCand: lstCand){
            if(objCand.Expected_Salary__c == null){
                objCand.addError('Please fill salary field');
            }
        }
    }
    
}