@isTest
public class CandidateTriggerTest {
    
    @testSetup static void methodName() {
        //single contact record
        Contact con = new Contact(LastName = 'ishwar shinde');
        insert con;
        
        //single job record
        Job__c jobdata = new Job__c();
        jobdata.Active__c = true;
        jobdata.Number_of_Positions__c = 5;
        jobdata.Salary_Offered__c =1000; 
        jobdata.Manager_assign__c =con.id;
        insert jobdata;
        
        //Bulk Candidate records
        List<Candidate__c> candBulkList=new List<Candidate__c>();       
        for(Integer i = 0 ; i<200 ; i++) {
           candBulkList.add(new Candidate__c(First_Name__c='Ishwar'+i,Last_Name__c='Pawar'+i,Email__c='ishwar'+i+'@gmail.com',Application_Date__c = null, Expected_Salary__c = i,Job__c = jobdata.id, Country__c = 'India',State__c = 'Maharashtra', Status__c = 'Hired'));
        }
        insert candBulkList;
    }
    
    @isTest public static void testAddDate(){
        Candidate__c cand = [SELECT Application_Date__c FROM Candidate__c WHERE First_Name__c=:'Ishwar0' LIMIT 1];    
        Test.startTest(); 
        try{
            cand.Application_Date__c = System.today();
            insert cand;
            Database.SaveResult result = Database.insert(cand, false);
        }catch(System.Exception e){
            String message = e.getMessage();
            //System.assert(message.contains('Assert failed.'));
        }
        Test.stopTest();
    }
    
    @isTest public static void testCheckActive(){
        Job__c nonActiveJob = [select Active__c from Job__c where Active__c=:true LIMIT 1];
        Candidate__c canddata = [SELECT Job__c FROM Candidate__c WHERE Job__c != NULL LIMIT 1];
        Test.startTest();      	
        try{
            nonActiveJob.Active__c = false;
            update nonActiveJob;
            Database.SaveResult result = Database.insert(nonActiveJob, false);
           // System.assertEquals('This Job is not active. You can not apply for this job',result.getErrors()[0].getMessage());
            
        }catch(DmlException e){
            String message = e.getMessage();
            system.assertEquals('This Job is not active. You can not apply for this job', message);
        }
        Test.stopTest();
    }
    
    @isTest public static void testCheckSalary(){
        Candidate__c cand = [select Expected_Salary__c from Candidate__c where Expected_Salary__c=:0 LIMIT 1];
        Test.startTest();      	
        try{
            cand.Expected_Salary__c = null;
            update cand;
            Database.SaveResult result = Database.insert(cand, false);
            System.assertEquals('Please fill salary field',result.getErrors()[0].getMessage());            
        }catch(Exception e){
            String message = e.getMessage();
            
        }
        
        try{
            List<Candidate__c> candList= [SELECT Expected_Salary__c FROM Candidate__c];
            System.assertEquals(200,candList.size());
        }
        catch(Exception e)
        {
            String message = e.getMessage();
            System.assert(message.contains('Assert failed.'));
        }
        Test.stopTest();
    }
}