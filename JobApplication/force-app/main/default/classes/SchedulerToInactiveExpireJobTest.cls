@isTest
public class SchedulerToInactiveExpireJobTest {
    @isTest public static void testExpireJob(){
        
        List<Job__c> jobdata = new List<Job__c>();
        Contact con = new Contact(LastName = 'ishwar shinde');
        insert con;
        List<Contact> contactId = [SELECT id FROM Contact WHERE LastName=:'ishwar shinde' LIMIT 1];
        for(Integer i=0; i < 200; i++){
            jobdata.add(new Job__c(Active__c=true,Number_of_Positions__c=i, Expires_On__c=System.today().addDays(-5), Name = 'a035g0000017V6A', Salary_Offered__c =1000+i, Manager_assign__c = contactId[0].id));
        }
        insert jobdata; 
        
        Map<Id, Job__c> jobmap = new Map<Id, Job__c>(jobdata);
        List<Id> jobIds = new List<Id>(jobmap.keySet());
        
        Test.startTest();
        // Schedule the test job
        String jobId = System.schedule('testScheduledApex',SchedulerToInactiveExpireJob.CRON_EXP, new SchedulerToInactiveExpireJob());
        List<Task> lt = [SELECT Id FROM Task WHERE WhatId IN :jobIds];  
        
        /*List <Job__c> expirejobdata = [SELECT Active__c, Expires_On__c FROM Job__c WHERE Expires_On__c < TODAY];
        for(Job__c expirejob : expirejobdata){
            if(expirejob.Expires_On__c < System.today()){
            expirejob.Active__c=false;
            update expirejob;
        }
        System.assertEquals(false, expirejob.Active__c);
        } */
        
        // Verify scheduled job has not run yet.
        System.assertEquals(0, lt.size(), 'Tasks exist before job has run');
        Test.stopTest();
        
        //Now the scheduled job has executed
        //check our tasks created or not
        List<Task> tasklt = [SELECT Id FROM Task WHERE WhatId IN :jobIds];
        //System.assertEquals(jobIds.size(), tasklt.size(),'Tasks were not created');
        System.assertEquals(200, jobdata.size());
    }
}