public class NewCandCustomController {


    public Candidate__c Candidate {get; private set;}

    
    public void NewCandCustomController(){
        Id id=ApexPages.currentPage().getParameters().get('id');
        Candidate = (id==null)? new Candidate__c():[select Application_Date__c, Country__c, Salutation__c, First_Name__c, Last_Name__c, Full_Name__c, DOB__c, Job__c, Email__c, State__c, Expected_Salary__c, Status__c from Candidate__c where Id=:id];
      }
    
        public PageReference save()
        {
            try{
                upsert(Candidate);
            }
            catch(System.DMLException e){
                ApexPages.addMessages(e);
                return null;    
            }
            PageReference redirectSuccess=new ApexPages.StandardController(Candidate).view();
            return redirectSuccess;
        }
}