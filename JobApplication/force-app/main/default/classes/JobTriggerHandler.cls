public class JobTriggerHandler {
    
    public static void jobActiveness(List<Job__c> lstJob){
        for(Job__c objJob : lstJob){
            if(objJob.Number_of_Positions__c == objJob.Hired_Applicants__c  && objJob.Active__c){
                objJob.Active__c=false;
            }else if (objJob.Number_of_Positions__c > objJob.Hired_Applicants__c  && !objJob.Active__c){
                objJob.Active__c=true;
            }
        }
    }
    
    public static void sentMail(List<Job__c> lstJob){  
        Set<Id> managerIds = new Set<Id>();
        
        for(Job__c objJob:lstJob) { 
            if(objJob.Number_of_Positions__c == objJob.Hired_Applicants__c && objJob.Manager_assign__c != null){
                managerIds.add(objJob.Manager_assign__c);
            }
        }
        
        Map<Id, Contact> idToContacts;
        if(!managerIds.isEmpty()){
            idToContacts = new  Map<Id, Contact>([Select Email,id,Name from Contact
                                              where id IN : managerIds]);
        }
        
        if(idToContacts == null || idToContacts.isEmpty()){
            return;
        }
		        
        Contact a, con;
        for (ID idKey : idToContacts.keyset()) {
            a = idToContacts.get(idKey);
            
        }
        
        for(Job__c objJob : lstJob){ 
            if(objJob.Number_of_Positions__c == objJob.Hired_Applicants__c){
                
                con = idToContacts.get(a.ID);
                
            }
        }
        
        for(Job__c objJob:lstJob){ 
            if(objJob.Number_of_Positions__c == objJob.Hired_Applicants__c){
                
                List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
                List<String> sendTo = new List<String>();
                sendTo.add(con.Email);
                mail.setToAddresses(sendTo);
                
                mail.setSubject('Reached Vaccancy Limit!..');
                String body = 'All required candidate has been hired for this job on '+ objJob.LastModifiedDate;
                
                mail.setHtmlBody(body);
                mails.add(mail);
                Messaging.sendEmail(mails);      
                
            }
            
        }
    }
    
    public static void jobStatus(List<Job__c> lstJob){
        for(Job__c objJob : lstJob){
            if(objJob.Active__c){
            	objJob.addError('This Job is active and can not be deleted');
        	}
        }
    }
}