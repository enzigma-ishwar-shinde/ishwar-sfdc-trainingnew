public class PracticeWork {
   		public void executeNew(){
        List<Job__c> joblists = new List<Job__c>();
        joblists = [ SELECT id, Active__c, Expires_On__c FROM Job__c WHERE Active__c = true AND Expires_On__c > TODAY ] ;
		System.debug('Before update' + joblists);
        for (Job__c jobdata : joblists){
            if(jobdata.Expires_On__c > System.today()){
                jobdata.Active__c=false;
                //jobdata.Expires_On__c=System.today()+1;
            }
        }
        update joblists;
        System.debug('After update' + joblists);
    }
}