public class NewJobCustomController {

    public String getCandidateRecords() {
        return null;
    }


    public Job__c Job {get; private set;}
    public List<Candidate__c> records{get;set;}
    public List<String> fields{get;set;}
    public List<Job__c> jobtype{get; set;}
    
    public NewJobCustomController(){
        Id id=ApexPages.currentPage().getParameters().get('id');
        Job = (id==null)? new Job__c():[select Name, Active__c, Certification_Required__c, Description__c, Expires_On__c, Hired_Applicants__c, Manager__c, Manager_assign__c, Name__c, Number_of_Positions__c, Qualification_Required__c, Required_Skills__c,Salary_Offered__c, Total_Applicants__c from Job__c where Id=:id];
        
       // List<Job__c> jobtype= [select Manager__c from Job__c where Id=:id ];
         
       // records=[select First_Name__c, Expected_Salary__c, Email__c from Candidate__c where cjob__c=:jobtype];
        
        records= [Select Name, Expected_Salary__c, Email__c FROM Candidate__c WHERE Id=:'a025g000002qUZW'];
       
        fields = new List<String>{'Name', 'Expected_Salary__c', 'Email__c'};
        
         

    }
    
        public PageReference save()
        {
            try{
                upsert(Job);
            }
            catch(System.DMLException e){
                ApexPages.addMessages(e);
                return null;    
            }
            PageReference redirectSuccess=new ApexPages.StandardController(Job).view();
            return redirectSuccess;
        }
}